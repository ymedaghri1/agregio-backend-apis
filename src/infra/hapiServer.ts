import Hapi from "@hapi/hapi"
import * as Inert from "@hapi/inert"
import Vision from "@hapi/vision"
import HapiSwagger from "hapi-swagger"
import { applicationRoutes } from "./routes"

const startServer = async () => {
  const server = Hapi.server({
    port: 3000,
    host: "localhost"
  })

  const swaggerOptions = {
    info: {
      title: "AGREGIO Test interview Documentation",
      version: "1.0"
    },
    grouping: "tags"
  }

  let plugins: any = [Inert, Vision, {
    plugin: HapiSwagger,
    options: swaggerOptions
  }]
  await server.register(plugins)

  applicationRoutes.map((route) => {
    server.route(route)
  })

  await server.start()
  console.log("Server running on %s", server.info.uri)

  return server
}

process.on("unhandledRejection", (err) => {
  console.log(err)
  process.exit(1)
})

export { startServer }
