import { ReqRefDefaults, Request, ResponseToolkit } from "@hapi/hapi"
import { Parc } from "../../domain/MarcheEnergie/Parc/Parc"
import ParcUseCases from "../../domain/MarcheEnergie/Parc/ParcUseCases"
import Boom from "@hapi/boom"

export default class ParcController {

  constructor(private readonly parcUseCases: ParcUseCases) {
  }

  findAllParcs() {
    return this.parcUseCases.getAllParcs()
  }

  findAParcById(request: Request<ReqRefDefaults>, hapiJsResponseToolkit: ResponseToolkit) {
    const { params: { id } } = request

    const parc = this.parcUseCases.getParcById(id)
    if (parc === undefined) {
      throw Boom.notFound("Parc introuvablr")
    }
    return hapiJsResponseToolkit.response(parc).code(200)
  }

  createAParc(request: any, hapiJsResponseToolkit: ResponseToolkit) {
    const { payload: { id, type } } = request
    const newParc = this.parcUseCases.create(new Parc(type, id))
    return hapiJsResponseToolkit.response(newParc).code(201)
  }
}
