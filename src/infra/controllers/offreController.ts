import OffreUseCases from "../../domain/MarcheEnergie/Offre/OffreUseCases"

export default class OffreController {

  constructor(private readonly offreUseCases: OffreUseCases) {
  }

  findAllOffres() {
    return this.offreUseCases.getAllOffres()
  }

}
