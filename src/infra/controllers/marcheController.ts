import { ResponseToolkit } from "@hapi/hapi"
import MarcheUseCases from "../../domain/MarcheEnergie/Marche/MarcheUseCases"

export default class MarcheController {

  constructor(private readonly marcheUseCases: MarcheUseCases) {
  }

  findAllMarches() {
    return this.marcheUseCases.getAllMarches()
  }

  placerOffre(request: any, hapiJsResponseToolkit: ResponseToolkit) {
    const { payload: { idOffre, typeMarche } } = request
    const marche = this.marcheUseCases.ajouterOffre(idOffre, typeMarche)
    return hapiJsResponseToolkit.response(marche).code(200)
  }

}
