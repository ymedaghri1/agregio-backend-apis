import { ServerRoute } from "@hapi/hapi"
import Joi from "joi"
import { parcController } from "../dependencies"
import { TypeParc } from "../../domain/MarcheEnergie/Parc/Parc"

const parcSchema = Joi.object({
  id: Joi.string().alphanum().required(),
  type: Joi.string().valid(...Object.keys(TypeParc).map(key => TypeParc[key as keyof typeof TypeParc])).required().label("type de parc"),
  calendrierBlocHoraires: Joi.object()
}).label("Parc")

const parcRoutes: ServerRoute[] = [
  {
    method: "GET",
    path: "/parcs",
    handler: parcController.findAllParcs.bind(parcController),
    options: {
      tags: ["api", "Parc"],
      description: "Lister tous les parcs",
      notes: "Retourne la liste de tous les parcs",
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              schema: Joi.array().items(parcSchema).label("Parcs")
            },
            404: {
              description: "Aucun parc trouvé"
            },
            500: {}
          }
        }
      }
    }
  },
  {
    method: "GET",
    path: "/parcs/{id}",
    handler: parcController.findAParcById.bind(parcController),
    options: {
      tags: ["api", "Parc"],
      description: "Consulter un parc",
      notes: "Retourne un parc à partir de son identifiant",
      validate: {
        params: Joi.object({
          id: Joi.string().required().description(`L'identifiant du parc recherché`)
        })
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              schema: parcSchema
            },
            404: {
              description: `Parc non trouvé`
            },
            500: {}
          }
        }
      }
    }
  },
  {
    method: "POST",
    path: "/parcs",
    handler: parcController.createAParc.bind(parcController),
    options: {
      tags: ["api", "Parc"],
      description: "Créer un parc",
      notes: "Créé un parc",
      validate: {
        payload: Joi.object({
          id: Joi.string().alphanum().required(),
          type: Joi.string().valid(...Object.keys(TypeParc).map(key => TypeParc[key as keyof typeof TypeParc])).required().label("type de parc")
        })
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            201: {
              schema: parcSchema
            },
            409: {
              description: "Conflit : Donnée déjà existante"
            },
            500: {}
          }
        }
      }
    }
  }
]

export default parcRoutes
