import { ServerRoute } from "@hapi/hapi"
import Joi from "joi"
import { marcheController } from "../dependencies"
import { TypeMarche } from "../../domain/MarcheEnergie/Marche/Marche"


const marcheSchema = Joi.object({
  type: Joi.string().valid(...Object.keys(TypeMarche).map(key => TypeMarche[key as keyof typeof TypeMarche])).required().label("type de marché"),
  offres: Joi.object()
}).label("Marché")

const marcheRoutes: ServerRoute[] = [
  {
    method: "GET",
    path: "/marches",
    handler: marcheController.findAllMarches.bind(marcheController),
    options: {
      tags: ["api", "Marché"],
      description: "Lister tous les marchés",
      notes: "Retourne la liste de tous les marchés",
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              schema: Joi.array().items(marcheSchema).label("Marchés")
            },
            404: {
              description: "Aucun marché trouvé"
            },
            500: {}
          }
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/marches/{type}",
    handler: marcheController.placerOffre.bind(marcheController),
    options: {
      tags: ["api", "Marché"],
      description: "Placer une offre sur un marché",
      notes: "Place une offre sur un marché",
      validate: {
        payload: Joi.object({
          idOffre: Joi.string().required(),
          typeMarche: Joi.string().valid(...Object.keys(TypeMarche).map(key => TypeMarche[key as keyof typeof TypeMarche])).required(),
        })
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              schema: marcheSchema
            },
            400: {
              description: "Requête invalide"
            },
            500: {}
          }
        }
      }
    }
  }
]

export default marcheRoutes
