import parcRoutes from "./parcRoutes"
import marcheRoutes from "./marcheRoutes"
import offreRoutes from "./offreRoutes"

export const applicationRoutes = [
  ...parcRoutes,
  ...marcheRoutes,
  ...offreRoutes
]
