import { ServerRoute } from "@hapi/hapi"
import Joi from "joi"
import { offreController } from "../dependencies"


const offreSchema = Joi.object({}).label("Offres")

const offreRoutes: ServerRoute[] = [
  {
    method: "GET",
    path: "/offres",
    handler: offreController.findAllOffres.bind(offreController),
    options: {
      tags: ["api", "Offres"],
      description: "Lister toutes les offres",
      notes: "Retourne la liste de toutes les offres",
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              schema: Joi.array().items(offreSchema).label("Offres")
            },
            404: {
              description: "Aucun marché trouvé"
            },
            500: {}
          }
        }
      }
    }
  }
]

export default offreRoutes
