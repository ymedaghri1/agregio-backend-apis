import InMemoryParcRepository from "../inMemoryRepositories/InMemoryParcRepository"
import ParcController from "../controllers/parcController"
import ParcUseCases from "../../domain/MarcheEnergie/Parc/ParcUseCases"
import MarcheController from "../controllers/marcheController"
import InMemoryMarcheRepository from "../inMemoryRepositories/InMemoryMarcheRepository"
import MarcheUseCases from "../../domain/MarcheEnergie/Marche/MarcheUseCases"
import InMemoryOffreRepository from "../inMemoryRepositories/InMemoryOffreRepository"
import OffreController from "../controllers/offreController"
import OffreUseCases from "../../domain/MarcheEnergie/Offre/OffreUseCases"

const parcRepository = new InMemoryParcRepository()
const parcUseCases = new ParcUseCases(parcRepository)
const parcController = new ParcController(parcUseCases)

const offreRepository = new InMemoryOffreRepository(parcRepository)

const marcheRepository = new InMemoryMarcheRepository()
const marcheUseCases = new MarcheUseCases(marcheRepository, offreRepository)
const marcheController = new MarcheController(marcheUseCases)


const offreUseCases = new OffreUseCases(offreRepository)
const offreController = new OffreController(offreUseCases)

export {
  parcController,
  marcheController,
  offreController
}
