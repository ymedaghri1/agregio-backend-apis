import { Marche, TypeMarche } from "../../domain/MarcheEnergie/Marche/Marche"
import { MarcheRepository } from "../../domain/MarcheEnergie/Marche/MarcheRepository"
import { Offre } from "../../domain/MarcheEnergie/Offre/Offre"

export default class InMemoryMarcheRepository implements MarcheRepository {

  constructor(private marches: Marche[] = [
    new Marche(TypeMarche.ReservePrimaire),
    new Marche(TypeMarche.ReserveSecondaire),
    new Marche(TypeMarche.ReserveRapide)]) {
  }

  findOne(type: TypeMarche): Marche | undefined {
    return this.marches.find(unMarche => unMarche.type === type)
  }

  findAll(): Marche[] {
    return this.marches
  }

  addOffre(offre: Offre, marche: Marche): Marche {
    marche.ajouterUneOffre(offre)
    return marche
  }

}
