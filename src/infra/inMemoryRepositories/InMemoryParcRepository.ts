import { ParcRepository } from "../../domain/MarcheEnergie/Parc/ParcRepository"
import { Parc, TypeParc } from "../../domain/MarcheEnergie/Parc/Parc"
import Boom from "@hapi/boom"
import { DateOnly, Mois } from "../../domain/MarcheEnergie/Offre/DateOnly"
import { BlocHoraireUnitaire } from "../../domain/MarcheEnergie/Parc/BlocHoraireUnitaire"

export default class InMemoryParcRepository implements ParcRepository {

  constructor(private parcs: Parc[] = []) {

    const _19_06_2023 = new DateOnly({ jour: 19, mois: Mois.Juin, annee: 2023 })
    const parcSolariux = new Parc(TypeParc.Solaire, "Solariux")
    for (let i = 6; i < 19; i++) {
      parcSolariux.ajouterBlocHoraireUnitaire(_19_06_2023, new BlocHoraireUnitaire(i, 10, 100))
    }

    const parcWinterstone = new Parc(TypeParc.Eolien, "WinterStone-1")
    for (let i = 0; i < 13; i++) {
      parcWinterstone.ajouterBlocHoraireUnitaire(_19_06_2023, new BlocHoraireUnitaire(i, 22, 220))
    }

    const parcPoseidon = new Parc(TypeParc.Hydraulique, "Poseidon-XA")
    for (let i = 12; i < 23; i++) {
      parcPoseidon.ajouterBlocHoraireUnitaire(_19_06_2023, new BlocHoraireUnitaire(i, 41, 405))
    }
    parcs.push(parcSolariux, parcWinterstone, parcPoseidon)
  }

  findAll(): Parc[] {
    return this.parcs
  }

  findOne(id: string): Parc | undefined {
    return this.parcs.find(unParc => unParc.id === id)
  }

  save(parc: Parc): Parc {
    const parcExistant = this.parcs.find(unParc => unParc.id === parc.id)
    if (parcExistant === undefined) {
      this.parcs.push(parc)
      return parc
    }

    throw Boom.conflict("Parc déjà existant")
  }

}
