import { Offre } from "../../domain/MarcheEnergie/Offre/Offre"
import { OffreRepository } from "../../domain/MarcheEnergie/Offre/OffreRepository"
import { DateOnly, Mois } from "../../domain/MarcheEnergie/Offre/DateOnly"
import { BlocHoraireOffre } from "../../domain/MarcheEnergie/Offre/BlocHoraireOffre"
import { ParcRepository } from "../../domain/MarcheEnergie/Parc/ParcRepository"

export default class InMemoryOffreRepository implements OffreRepository {

  constructor(private parcRepository: ParcRepository, private offres: Offre[] = []) {
    const _19_06_2023 = new DateOnly({ jour: 19, mois: Mois.Juin, annee: 2023 })
    const parcSolariux = parcRepository.findOne("Solariux")!
    const parcWinterStone = parcRepository.findOne("WinterStone-1")!
    const blocsHoraire = [
      new BlocHoraireOffre(0, 2, parcWinterStone, _19_06_2023),
      new BlocHoraireOffre(3, 5, parcWinterStone, _19_06_2023),
      new BlocHoraireOffre(6, 8, parcSolariux, _19_06_2023),
      new BlocHoraireOffre(9, 11, parcSolariux, _19_06_2023),
      new BlocHoraireOffre(12, 14, parcSolariux, _19_06_2023),
      new BlocHoraireOffre(15, 17, parcSolariux, _19_06_2023)
    ]
    offres.push(new Offre("OFR_1", _19_06_2023, blocsHoraire))
  }

  findAll(): Offre[] {
    return this.offres
  }

  findOne(id: string): Offre | undefined {
    return this.offres.find(uneOffre => uneOffre.id === id)
  }

}
