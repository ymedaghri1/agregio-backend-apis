import {
  BlocHoraireUnitaireError,
  BlocHoraireUnitaireErrorConstants,
} from "./errors/BlocHoraireUnitaireError"

export class BlocHoraireUnitaire {
  constructor(
    readonly creneauHoraire: number,
    readonly quantiteEnergieMW: number,
    readonly prixEuros: number,
  ) {
    if (creneauHoraire < 0 || creneauHoraire>23) {
      throw new BlocHoraireUnitaireError(
        BlocHoraireUnitaireErrorConstants.CreneauHoraireInvalide
      )
    }
  }
}
