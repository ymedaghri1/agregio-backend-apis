import { BlocHoraireUnitaire } from "./BlocHoraireUnitaire"
import { expect } from "chai"
import { BlocHoraireUnitaireError } from "./errors/BlocHoraireUnitaireError"

describe("Bloc Horaire Unitaire", () => {
  const quantiteEnergie = 10
  const prix = 2000

  describe("Plage Horaire", () => {
    it(`doit renvoyer une erreur de CreneauHoraireInvalide sur un bloc horaire dont le créneau horaire est < 0`, function() {
      // Given
      const creneauHoraire = -1

      // When
      const nouveauBlocHoraire = () =>
        new BlocHoraireUnitaire(creneauHoraire, quantiteEnergie, prix)

      // Then
      expect(nouveauBlocHoraire).to.throw(
        BlocHoraireUnitaireError,
        "BlocHoraireUnitaireError : CreneauHoraireInvalide"
      )
    })
    it(`doit renvoyer une erreur de CreneauHoraireInvalide sur un bloc horaire dont le créneau horaire est > 23`, function() {
      // Given
      const creneauHoraire = 24

      // When
      const nouveauBlocHoraire = () =>
        new BlocHoraireUnitaire(creneauHoraire, quantiteEnergie, prix)

      // Then
      expect(nouveauBlocHoraire).to.throw(
        BlocHoraireUnitaireError,
        "BlocHoraireUnitaireError : CreneauHoraireInvalide"
      )
    })
  })
})
