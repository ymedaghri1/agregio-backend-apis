import { BlocHoraireUnitaire } from "./BlocHoraireUnitaire"
import { DateOnly, JoursDeLaSemaine } from "../Offre/DateOnly"
import { ParcError, ParcErrorConstants } from "./errors/ParcError"

export enum TypeParc {
  Solaire = 'Solaire',
  Eolien = 'Eolien',
  Hydraulique = 'Hydraulique'
}


export class Parc {
  constructor(
    readonly type: TypeParc,
    readonly id: string,
    private calendrierBlocHoraires: Record<string, (BlocHoraireUnitaire)[]> = {}
  ) {
  }

  ajouterBlocHoraireUnitaire(dateOnly: DateOnly, blocHoraire: BlocHoraireUnitaire) {
    const blocsHoraireUnitaires: (BlocHoraireUnitaire|undefined)[] = this.calendrierBlocHoraires[dateOnly.id] ?? []
    blocsHoraireUnitaires[blocHoraire.creneauHoraire] = blocHoraire
    this.calendrierBlocHoraires[dateOnly.id] = blocsHoraireUnitaires.filter(b=>b!==undefined) as BlocHoraireUnitaire[]
  }

  ajouterBlocsHoraireUnitairesEntreDeuxDates(dateDebut: DateOnly, dateFin: DateOnly, blocHoraire: BlocHoraireUnitaire) {
    if (!dateDebut.avant(dateFin)) {
      throw new ParcError(ParcErrorConstants.IntervalleDeDateIncorrect)
    }
    for (const date of this.dateGenerator(dateDebut, dateFin)) {
      this.ajouterBlocHoraireUnitaire(date, blocHoraire)
    }
  }

  ajouterBlocsHoraireAvecRecurrenceJournaliere(dateDebut: DateOnly, dateFin: DateOnly, jourDeLaSemaine: JoursDeLaSemaine, blocHoraire: BlocHoraireUnitaire) {
    if (!dateDebut.avant(dateFin)) {
      throw new ParcError(ParcErrorConstants.IntervalleDeDateIncorrect)
    }
    for (const date of this.dateGenerator(dateDebut, dateFin)) {
      if (date.date.getDay() == jourDeLaSemaine) {
        this.ajouterBlocHoraireUnitaire(date, blocHoraire)
      }
    }
  }

  * dateGenerator(dateDebut: DateOnly, dateFin: DateOnly) {
    const dateCourante = new Date(dateDebut.date)

    while (dateCourante <= dateFin.date) {
      yield new DateOnly({
        jour: dateCourante.getDate(),
        mois: dateCourante.getMonth(),
        annee: dateCourante.getFullYear()
      })
      dateCourante.setDate(dateCourante.getDate() + 1)
    }
  }

  listerBlocsHoraires(dateOnly: DateOnly): (BlocHoraireUnitaire)[] {
    return this.calendrierBlocHoraires[dateOnly.id] ?? []
  }
}
