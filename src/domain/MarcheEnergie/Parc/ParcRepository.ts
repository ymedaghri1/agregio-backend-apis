import { Parc } from "./Parc"

export interface ParcRepository {
  findAll(): Parc[]
  findOne(id:string): Parc|undefined
  save(parc:Parc): Parc
}
