import { expect } from "chai"
import { Parc, TypeParc } from "./Parc"
import { DateOnly, JoursDeLaSemaine, Mois } from "../Offre/DateOnly"
import { BlocHoraireUnitaire } from "./BlocHoraireUnitaire"

describe("Parc", () => {

  const idParc = "Commwatt-1"

  it(`doit instancier un parc avec son type et un calendrierBlocHoraires vide`, function() {
    // Given - When
    const parcSolaire = new Parc(TypeParc.Solaire, idParc)

    // Then
    expect(parcSolaire).to.deep.equal({
      type: TypeParc.Solaire,
      id: "Commwatt-1",
      calendrierBlocHoraires: {}
    })
  })

  it(`peut ajouter un blocHoraire à un parc à une date donnée`, function() {
    // Given
    const parcEolien = new Parc(TypeParc.Eolien, idParc)
    const _10_01_2023 = new DateOnly({ jour: 10, mois: Mois.Janvier, annee: 2023 })
    const blocHoraire = new BlocHoraireUnitaire(0, 12, 120)

    // When
    parcEolien.ajouterBlocHoraireUnitaire(_10_01_2023, blocHoraire)
    const blocHoraireUnitaires = parcEolien.listerBlocsHoraires(_10_01_2023)

    // Then
    expect(parcEolien.type).to.equal(TypeParc.Eolien)
    expect(blocHoraireUnitaires).to.have.lengthOf(1)
    expect(blocHoraireUnitaires).to.deep.equal([blocHoraire])
  })

  it(`peut ajouter plusieurs blocsHoraire à un parc à une date donnée`, function() {
    // Given
    const parcEolien = new Parc(TypeParc.Eolien, idParc)
    const _10_01_2023 = new DateOnly({ jour: 10, mois: Mois.Janvier, annee: 2023 })
    const blocHoraire_0 = new BlocHoraireUnitaire(0, 12, 120)
    const blocHoraire_10 = new BlocHoraireUnitaire(10, 20, 200)
    const blocHoraire_22 = new BlocHoraireUnitaire(22, 30, 240)

    // When
    parcEolien.ajouterBlocHoraireUnitaire(_10_01_2023, blocHoraire_0)
    parcEolien.ajouterBlocHoraireUnitaire(_10_01_2023, blocHoraire_10)
    parcEolien.ajouterBlocHoraireUnitaire(_10_01_2023, blocHoraire_22)

    const blocHoraireUnitaires = parcEolien.listerBlocsHoraires(_10_01_2023)

    // Then
    expect(parcEolien.type).to.equal(TypeParc.Eolien)
    expect(blocHoraireUnitaires).to.have.lengthOf(3)
    expect(blocHoraireUnitaires).to.deep.equal([blocHoraire_0, blocHoraire_10, blocHoraire_22 ])
  })

  it(`peut ajouter un blocHoraire tous les jours entre deux dates`, function() {
    // Given
    const parcEolien = new Parc(TypeParc.Eolien, idParc)
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const _19_06_2023 = new DateOnly({ jour: 19, mois: Mois.Juin, annee: 2023 })
    const _20_06_2023 = new DateOnly({ jour: 20, mois: Mois.Juin, annee: 2023 })
    const blocHoraire = new BlocHoraireUnitaire(0, 12, 120)

    // When
    parcEolien.ajouterBlocsHoraireUnitairesEntreDeuxDates(_18_06_2023, _20_06_2023, blocHoraire)
    const blocHoraireUnitaires_18_06_2023 = parcEolien.listerBlocsHoraires(_18_06_2023)
    const blocHoraireUnitaires_19_06_2023 = parcEolien.listerBlocsHoraires(_19_06_2023)
    const blocHoraireUnitaires_20_06_2023 = parcEolien.listerBlocsHoraires(_20_06_2023)

    // Then
    expect(parcEolien.type).to.equal(TypeParc.Eolien)
    expect(blocHoraireUnitaires_18_06_2023).to.have.lengthOf(1)
    expect(blocHoraireUnitaires_19_06_2023).to.have.lengthOf(1)
    expect(blocHoraireUnitaires_20_06_2023).to.have.lengthOf(1)

    expect(blocHoraireUnitaires_18_06_2023).to.deep.equal([blocHoraire])
    expect(blocHoraireUnitaires_19_06_2023).to.deep.equal([blocHoraire])
    expect(blocHoraireUnitaires_20_06_2023).to.deep.equal([blocHoraire])
  })

  it(`peut ajouter un blocHoraire certains jours précis entre deux dates`, function() {
    // Given
    const parcEolien = new Parc(TypeParc.Eolien, idParc)
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const _19_06_2023 = new DateOnly({ jour: 19, mois: Mois.Juin, annee: 2023 })
    const _26_06_2023 = new DateOnly({ jour: 26, mois: Mois.Juin, annee: 2023 })
    const _03_07_2023 = new DateOnly({ jour: 3, mois: Mois.Juillet, annee: 2023 })
    const _04_07_2023 = new DateOnly({ jour: 4, mois: Mois.Juillet, annee: 2023 })
    const blocHoraire = new BlocHoraireUnitaire(0, 12, 120)

    // When
    parcEolien.ajouterBlocsHoraireAvecRecurrenceJournaliere(_18_06_2023, _04_07_2023, JoursDeLaSemaine.Lundi, blocHoraire)
    const blocHoraireUnitaires_18_06_2023 = parcEolien.listerBlocsHoraires(_18_06_2023)
    const blocHoraireUnitaires_19_06_2023 = parcEolien.listerBlocsHoraires(_19_06_2023)
    const blocHoraireUnitaires_26_06_2023 = parcEolien.listerBlocsHoraires(_26_06_2023)
    const blocHoraireUnitaires_03_07_2023 = parcEolien.listerBlocsHoraires(_03_07_2023)
    const blocHoraireUnitaires_04_07_2023 = parcEolien.listerBlocsHoraires(_04_07_2023)

    // Then
    expect(parcEolien.type).to.equal(TypeParc.Eolien)
    expect(blocHoraireUnitaires_19_06_2023).to.have.lengthOf(1)
    expect(blocHoraireUnitaires_19_06_2023).to.deep.equal([blocHoraire])

    expect(blocHoraireUnitaires_26_06_2023).to.have.lengthOf(1)
    expect(blocHoraireUnitaires_26_06_2023).to.deep.equal([blocHoraire])

    expect(blocHoraireUnitaires_03_07_2023).to.have.lengthOf(1)
    expect(blocHoraireUnitaires_03_07_2023).to.deep.equal([blocHoraire])

    expect(blocHoraireUnitaires_18_06_2023).to.have.lengthOf(0)

    expect(blocHoraireUnitaires_04_07_2023).to.have.lengthOf(0)
  })

})
