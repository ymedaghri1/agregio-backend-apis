import { ParcRepository } from "./ParcRepository"
import { Parc } from "./Parc"


export default class ParcUseCases {
  constructor(private parcRepository: ParcRepository) {
  }

  create(parc:Parc):Parc {
    return this.parcRepository.save(parc)
  }

  getAllParcs(): Parc[] {
    return this.parcRepository.findAll()
  }

  getParcById(id:string): Parc|undefined {
    return this.parcRepository.findOne(id)
  }
}
