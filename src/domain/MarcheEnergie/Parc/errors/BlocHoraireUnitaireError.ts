export enum BlocHoraireUnitaireErrorConstants {
  CreneauHoraireInvalide = "CreneauHoraireInvalide"
}

export class BlocHoraireUnitaireError extends Error {
  constructor(readonly type: BlocHoraireUnitaireErrorConstants) {
    super(`BlocHoraireUnitaireError : ${type}`)

    Object.setPrototypeOf(this, BlocHoraireUnitaireError.prototype)
  }
}
