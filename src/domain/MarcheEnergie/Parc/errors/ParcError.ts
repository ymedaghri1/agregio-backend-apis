export enum ParcErrorConstants {
  IntervalleDeDateIncorrect = "IntervalleDeDateIncorrect",
}

export class ParcError extends Error {
  constructor(public readonly type: ParcErrorConstants) {
    super(`ParcError : ${type}`)

    Object.setPrototypeOf(this, ParcError.prototype)
  }
}
