import { BlocHoraireUnitaireError, BlocHoraireUnitaireErrorConstants } from "./BlocHoraireUnitaireError"
import { expect } from "chai"

describe("BlocHoraireUnitaireError", () => {
  it("doit instancier une erreur BlocHoraireUnitaireError avec le champ type correspondant au détail de l'erreur", () => {
    for (const key of Object.keys(BlocHoraireUnitaireErrorConstants)) {

      const enumValue = BlocHoraireUnitaireErrorConstants[key as keyof typeof BlocHoraireUnitaireErrorConstants]
      // Given - When
      const blocHoraireUnitaireError = new BlocHoraireUnitaireError(
        enumValue
      )
      // Then
      expect(blocHoraireUnitaireError.type).to.equal(
        enumValue
      )
      expect(blocHoraireUnitaireError.message).to.equal(
        `BlocHoraireUnitaireError : ${enumValue}`
      )
    }
  })
})
