import { DateOnlyError, DateOnlyErrorConstants } from "./errors/DateOnlyError"

export enum Mois {
  Janvier = 0,
  Fevrier,
  Mars,
  Avril,
  Mai,
  Juin,
  Juillet,
  Aout,
  Septembre,
  Octobre,
  Novembre,
  Decembre,
}

export enum JoursDeLaSemaine {
  Dimanche = 0,
  Lundi,
  Mardi,
  Mercredi,
  Jeudi,
  Vendredi,
  Samedi,
}

export class DateOnly {
  public readonly date: Date

  get id(): string {
    return `${this.date.getFullYear()}-${this.date.getMonth()}-${this.date.getDate()}`
  }

  constructor({ jour, annee, mois }: { jour: number; annee: number; mois: Mois }) {
    const date = new Date(Date.UTC(annee, mois, jour))
    if (date.getUTCFullYear() !== annee || date.getUTCMonth() !== mois || date.getUTCDate() !== jour) {
      throw new DateOnlyError(DateOnlyErrorConstants.DateInvalide)
    }
    this.date = date
  }

  avant(dateFin: DateOnly) {
    return this.date < dateFin.date
  }

}
