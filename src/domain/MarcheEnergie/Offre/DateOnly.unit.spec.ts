import { expect } from "chai"
import { DateOnly, Mois } from "./DateOnly"
import { DateOnlyError } from "./errors/DateOnlyError"

describe("DateOnly", () => {

  it(`doit instancier un objet DateOnly valide`, function() {
    // Given - When
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })

    // Then
    expect(_18_06_2023.id).to.deep.equal("2023-5-18")
  })

  it(`doit renvpyer une erreur si on souhaite instancier un objet DateOnly invalide`, function() {
    // Given - When
    const dateConstruction = () => new DateOnly({ jour: 34, mois: Mois.Juin, annee: 2023 })

    // Then
    expect(dateConstruction).to.throw(
      DateOnlyError,
      "DateOnlyError : DateInvalide"
    )
  })

  it(`doit renvoyer true si la date de début est avant la date de fin`, function() {
    // Given - When
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const _19_06_2023 = new DateOnly({ jour: 19, mois: Mois.Juin, annee: 2023 })

    // Then
    expect(_18_06_2023.avant(_19_06_2023)).to.be.true
  })

  it(`doit renvoyer false si la date de début est égale à la date de fin`, function() {
    // Given - When
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })

    // Then
    expect(_18_06_2023.avant(_18_06_2023)).to.be.false
  })

  it(`doit renvoyer false si la date de début est après la date de fin`, function() {
    // Given - When
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const _19_06_2023 = new DateOnly({ jour: 19, mois: Mois.Juin, annee: 2023 })

    // Then
    expect(_19_06_2023.avant(_18_06_2023)).to.be.false
  })

})
