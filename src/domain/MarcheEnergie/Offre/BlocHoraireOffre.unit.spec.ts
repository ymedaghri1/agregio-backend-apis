import { expect } from "chai"
import { BlocHoraireOffre } from "./BlocHoraireOffre"
import { BlocHoraireOffreError } from "./errors/BlocHoraireOffreError"
import { DateOnly, Mois } from "./DateOnly"
import { Parc, TypeParc } from "../Parc/Parc"
import { BlocHoraireUnitaire } from "../Parc/BlocHoraireUnitaire"

describe("Bloc Horaire Offre", () => {
  const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
  const parc = PARC(_18_06_2023)

  describe("Plage Horaire", () => {
    it(`doit renvoyer une erreur de plageHoraireInvalide sur un bloc horaire offre dont la date de début est < 0`, function() {
      // Given
      const creneauHoraireDebut = -1
      const creneauHoraireFin = 12

      // When
      const blocHoraireConstruction = () =>
        new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, parc, _18_06_2023)

      // Then
      expect(blocHoraireConstruction).to.throw(
        BlocHoraireOffreError,
        "BlocHoraireOffreError : PlageHoraireInvalide"
      )
    })
    it(`doit renvoyer une erreur de plageHoraireInvalide sur un bloc horaire dont la date de fin est > 23`, function() {
      // Given
      const creneauHoraireDebut = 5
      const creneauHoraireFin = 24

      // When
      const blocHoraireConstruction = () =>
        new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, parc, _18_06_2023)

      // Then
      expect(blocHoraireConstruction).to.throw(
        BlocHoraireOffreError,
        "BlocHoraireOffreError : PlageHoraireInvalide"
      )
    })
    it(`doit renvoyer une erreur de plageHoraireInvalide si la date de début n'est pas strictement inférieure à la date de fin`, function() {
      // Given
      const creneauHoraireDebut = 15
      const creneauHoraireFin = 10

      // When
      const blocHoraireConstruction = () =>
        new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, parc, _18_06_2023)

      // Then
      expect(blocHoraireConstruction).to.throw(
        BlocHoraireOffreError,
        "BlocHoraireOffreError : PlageHoraireInvalide"
      )
    })
    it(`doit renvoyer une erreur de BlocsHorairesProducteurIncompatibles sur un bloc horaire offre qui mentionne des blocs horaires non renseignés par le producteur`, function() {
      // Given
      const parcDiurne = PARC_DIURNE(_18_06_2023)

      const creneauHoraireDebut = 4
      const creneauHoraireFin = 10

      // When
      const tentativeBlocHoraire = () => new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, parcDiurne, _18_06_2023)

      // Then
      expect(tentativeBlocHoraire).to.throw(
        BlocHoraireOffreError,
        "BlocHoraireOffreError : BlocsHorairesProducteurIncompatibles"
      )

    })
    it(`ne doit pas renvoyer d'erreurs si les dates de début et de fin sont comprises entre 0 et 24 et que la date de début n'est pas strictement inférieure à la date de fin`, function() {
      for (let i = 0; i < 23; i++) {
        // Given
        const creneauHoraireDebut = i
        const creneauHoraireFin = creneauHoraireDebut + 1

        // When
        const blocHoraire = new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, parc, _18_06_2023)

        // Then
        expect(blocHoraire).to.deep.equal({
          creneauHoraireDebut,
          creneauHoraireFin,
          _idParc: "Icare_02",
          _prixPlancher: 120,
          _quantite: 12
        })
      }
    })
    it(`doit calculer le prix plancher et la quantité d'un blocHoraire offre`, function() {
      // Given
      const creneauHoraireDebut = 10
      const creneauHoraireFin = 12

      // When
      const blocHoraire = new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, parc, _18_06_2023)

      // Then
      expect(blocHoraire.prixPlancher).to.equal(240)
      expect(blocHoraire.quantite).to.equal(24)

    })

  })
})

function PARC(dateProduction: DateOnly) {
  const parcEolien = new Parc(TypeParc.Eolien, "Icare_02")
  for (let i = 0; i < 24; i++) {
    const blocHoraire = new BlocHoraireUnitaire(i, 12, 120)
    parcEolien.ajouterBlocHoraireUnitaire(dateProduction, blocHoraire)
  }
  return parcEolien
}

function PARC_DIURNE(dateProduction: DateOnly) {
  const parcEolien = new Parc(TypeParc.Eolien, "Icare_02")
  for (let i = 8; i < 20; i++) {
    const blocHoraire = new BlocHoraireUnitaire(i, 12, 120)
    parcEolien.ajouterBlocHoraireUnitaire(dateProduction, blocHoraire)
  }
  return parcEolien
}
