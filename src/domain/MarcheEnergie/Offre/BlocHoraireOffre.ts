import { BlocHoraireOffreError, BlocHoraireOffreErrorConstants } from "./errors/BlocHoraireOffreError"
import { Parc } from "../Parc/Parc"
import { DateOnly } from "./DateOnly"
import { BlocHoraireUnitaire } from "../Parc/BlocHoraireUnitaire"

export class BlocHoraireOffre {
  private _prixPlancher: number = 0
  private _quantite: number = 0
  private _idParc: string

  get idParc(): string {
    return this._idParc
  }

  get prixPlancher(): number {
    return this._prixPlancher
  }

  get quantite(): number {
    return this._quantite
  }

  constructor(
    readonly creneauHoraireDebut: number,
    readonly creneauHoraireFin: number,
    parc: Parc,
    dateOffre: DateOnly) {
    if (creneauHoraireDebut < 0 || creneauHoraireFin > 23 || creneauHoraireDebut >= creneauHoraireFin) {
      throw new BlocHoraireOffreError(
        BlocHoraireOffreErrorConstants.PlageHoraireInvalide
      )
    }

    let prixPlancher = 0
    let quantite = 0


    const blocsHoraireParcDict = parc.listerBlocsHoraires(dateOffre)
      .reduce((dict:Record<number, BlocHoraireUnitaire>, current: BlocHoraireUnitaire) => {
        dict[current.creneauHoraire] = current
        return dict
      }, {})

    for (let i = creneauHoraireDebut; i < creneauHoraireFin; i++) {
      if (blocsHoraireParcDict[i] === undefined) {
        throw new BlocHoraireOffreError(
          BlocHoraireOffreErrorConstants.BlocsHorairesProducteurIncompatibles
        )
      }

      prixPlancher += blocsHoraireParcDict[i]!.prixEuros
      quantite += blocsHoraireParcDict[i]!.quantiteEnergieMW
    }

    this._quantite = quantite
    this._prixPlancher = prixPlancher
    this._idParc = parc.id
  }
}
