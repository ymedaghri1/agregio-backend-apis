import { DateOnly } from "./DateOnly"
import { Parc } from "../Parc/Parc"
import { BlocHoraireOffre } from "./BlocHoraireOffre"
import { OffreError, OffreErrorConstants } from "./errors/OffreError"


export class Offre {

  constructor(
    readonly id: string,
    private readonly dateOffre: DateOnly,
    private blocsHoraire: BlocHoraireOffre[] = []
  ) {

  }

  ajouterBlocHoraire(creneauHoraireDebut: number, creneauHoraireFin: number, producteur: Parc) {
    const nouveauBlocHoraire = new BlocHoraireOffre(creneauHoraireDebut, creneauHoraireFin, producteur, this.dateOffre)
    this.checkChevauchementDeBlocsHoraires(nouveauBlocHoraire)
    this.blocsHoraire.push(nouveauBlocHoraire)
  }

  private checkChevauchementDeBlocsHoraires(nouveauBlocHoraire: BlocHoraireOffre) {
    for (const blocHoraire of this.blocsHoraire) {
      if (
        nouveauBlocHoraire.creneauHoraireFin >= blocHoraire.creneauHoraireDebut && nouveauBlocHoraire.creneauHoraireDebut <= blocHoraire.creneauHoraireFin
      ) {
        throw new OffreError(OffreErrorConstants.BlocsHorairesSuperposes)
      }
    }
  }
}
