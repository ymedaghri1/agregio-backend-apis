import { Offre } from "../Offre/Offre"

export interface OffreRepository {
  findOne(id: string): Offre | undefined

  findAll(): Offre[]
}
