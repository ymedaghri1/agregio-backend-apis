import { OffreRepository } from "../Offre/OffreRepository"
import { Offre } from "./Offre"

export default class OffreUseCases {
  constructor(private offreRepository: OffreRepository) {
  }

  getAllOffres(): Offre[] {
    return this.offreRepository.findAll()
  }

}
