export enum OffreErrorConstants {
  BlocsHorairesSuperposes = "BlocsHorairesSuperposes",
}

export class OffreError extends Error {
  constructor(public readonly type: OffreErrorConstants) {
    super(`OffreError : ${type}`)

    Object.setPrototypeOf(this, OffreError.prototype)
  }
}
