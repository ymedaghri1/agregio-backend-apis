export enum BlocHoraireOffreErrorConstants {
  PlageHoraireInvalide = "PlageHoraireInvalide",
  BlocsHorairesProducteurIncompatibles = "BlocsHorairesProducteurIncompatibles",
}

export class BlocHoraireOffreError extends Error {
  constructor(public readonly type: BlocHoraireOffreErrorConstants) {
    super(`BlocHoraireOffreError : ${type}`)

    Object.setPrototypeOf(this, BlocHoraireOffreError.prototype)
  }
}
