export enum DateOnlyErrorConstants {
  DateInvalide = "DateInvalide",
}

export class DateOnlyError extends Error {
  constructor(public readonly type: DateOnlyErrorConstants) {
    super(`DateOnlyError : ${type}`)

    Object.setPrototypeOf(this, DateOnlyError.prototype)
  }
}
