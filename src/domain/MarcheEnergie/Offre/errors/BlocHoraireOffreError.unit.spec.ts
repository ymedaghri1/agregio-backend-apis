import { expect } from "chai"
import { BlocHoraireOffreError, BlocHoraireOffreErrorConstants } from "./BlocHoraireOffreError"

describe("BlocHoraireOffreError", () => {
  it("doit instancier une erreur BlocHoraireOffreError avec le champ type correspondant au détail de l'erreur", () => {
    for (const key of Object.keys(BlocHoraireOffreErrorConstants)) {

      const enumValue = BlocHoraireOffreErrorConstants[key as keyof typeof BlocHoraireOffreErrorConstants]
      // Given - When
      const blocHoraireOffreError = new BlocHoraireOffreError(
        enumValue
      )
      // Then
      expect(blocHoraireOffreError.type).to.equal(
        enumValue
      )
      expect(blocHoraireOffreError.message).to.equal(
        `BlocHoraireOffreError : ${enumValue}`
      )
    }
  })
})
