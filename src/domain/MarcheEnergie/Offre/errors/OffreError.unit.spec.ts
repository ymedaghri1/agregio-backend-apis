import { expect } from "chai"
import { OffreError, OffreErrorConstants } from "./OffreError"

describe("OffreError", () => {
  it("doit instancier une erreur OffreError avec le champ type correspondant au détail de l'erreur", () => {
    for (const key of Object.keys(OffreErrorConstants)) {

      const enumValue = OffreErrorConstants[key as keyof typeof OffreErrorConstants]
      // Given - When
      const offreError = new OffreError(
        enumValue
      )
      // Then
      expect(offreError.type).to.equal(
        enumValue
      )
      expect(offreError.message).to.equal(
        `OffreError : ${enumValue}`
      )
    }
  })
})
