import { expect } from "chai"
import { Marche, TypeMarche } from "../Marche/Marche"
import { Offre } from "./Offre"
import { DateOnly, Mois } from "./DateOnly"
import { Parc, TypeParc } from "../Parc/Parc"
import { BlocHoraireUnitaire } from "../Parc/BlocHoraireUnitaire"
import { OffreError } from "./errors/OffreError"

describe("Offre", () => {
  const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })

  it(`peut instancier une offre à une date donnée avec un id donné`, function() {
    // Given - When
    const offre = new Offre("OFR_1", _18_06_2023)

    // Then
    expect(offre).to.deep.equal({
      blocsHoraire: [],
      dateOffre: _18_06_2023,
      id: "OFR_1"
    })
  })

  it(`peut ajouter des blocs horaires qui ne se chevauchent pas`, function() {
    // Given
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const offre = new Offre("OFR_1", _18_06_2023)
    const producteur = PRODUCTEUR(_18_06_2023)

    // When
    offre.ajouterBlocHoraire(0, 2, producteur)
    offre.ajouterBlocHoraire(3, 5, producteur)

    // Then
    expect(offre).to.deep.equal({
      blocsHoraire: [
        {
          creneauHoraireDebut: 0,
          creneauHoraireFin: 2,
          _idParc: "Icare_02",
          _prixPlancher: 240,
          _quantite: 24
        },
        {
          creneauHoraireDebut: 3,
          creneauHoraireFin: 5,
          _idParc: "Icare_02",
          _prixPlancher: 240,
          _quantite: 24
        }
      ],
      dateOffre: _18_06_2023,
      id: "OFR_1"
    })
  })

  it(`ne peut pas ajouter des blocs horaires qui se chevauchent`, function() {
    // Given
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const offre = new Offre("OFR_1", _18_06_2023)
    const producteur = PRODUCTEUR(_18_06_2023)

    // When
    offre.ajouterBlocHoraire(5, 7, producteur)
    const tentativeAjout = ()=>offre.ajouterBlocHoraire(1, 6, producteur)

    // Then
    expect(tentativeAjout).to.throw(
      OffreError,
      "OffreError : BlocsHorairesSuperposes"
    )
  })

})

function PRODUCTEUR(dateProduction: DateOnly) {
  const producteurEolien = new Parc(TypeParc.Eolien, "Icare_02")
  for (let i = 0; i < 24; i++) {
    const blocHoraire = new BlocHoraireUnitaire(i, 12, 120)
    producteurEolien.ajouterBlocHoraireUnitaire(dateProduction, blocHoraire)
  }
  return producteurEolien
}
