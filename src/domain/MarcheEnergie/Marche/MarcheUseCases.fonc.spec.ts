import { expect } from "chai"
import { Server } from "@hapi/hapi"
import { startServer } from "../../../infra/hapiServer"


describe("GET /parcs/{id}", () => {
  let server: Server

  beforeEach(async () => {
    server = await startServer()
  })

  afterEach(async () => {
    await server.stop()
  })

  it("Recherche un parc à partir se son identifiant", async () => {
    const injectOptions = {
      method: "GET",
      url: "/parcs/WinterStone-1"
    }

    const response = await server.inject(injectOptions)

    expect(response.statusCode).to.equal(200)
    const payload = JSON.parse(response.payload)
    expect(payload.id).to.equal("WinterStone-1")

  })
})
