import { expect } from "chai"
import { Marche, TypeMarche } from "./Marche"
import { Offre } from "../Offre/Offre"
import { DateOnly, Mois } from "../Offre/DateOnly"
import { Parc, TypeParc } from "../Parc/Parc"
import { BlocHoraireUnitaire } from "../Parc/BlocHoraireUnitaire"

describe("Marche", () => {

  it(`doit instancier un marché avec son type`, function() {
    // Given - When
    const reservePrimaire = new Marche(TypeMarche.ReservePrimaire)

    // Then
    expect(reservePrimaire).to.deep.equal({
      type: TypeMarche.ReservePrimaire,
      offres: []
    })
  })
  it(`doit ajouter une offre sur un marché`, function() {
    // Given
    const reservePrimaire = new Marche(TypeMarche.ReservePrimaire)
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const parcEolien = PARC_EOLIEN(_18_06_2023)
    const parcSolaire = PARC_SOLAIRE(_18_06_2023)
    const offre = new Offre("OFR_1", _18_06_2023)
    offre.ajouterBlocHoraire(0, 2, parcEolien)
    offre.ajouterBlocHoraire(3, 5, parcEolien)
    offre.ajouterBlocHoraire(6, 8, parcSolaire)
    offre.ajouterBlocHoraire(9, 11, parcSolaire)
    offre.ajouterBlocHoraire(12, 14, parcSolaire)
    offre.ajouterBlocHoraire(15, 17, parcSolaire)
    offre.ajouterBlocHoraire(18, 20, parcEolien)
    offre.ajouterBlocHoraire(21, 23, parcEolien)


    // When
    reservePrimaire.ajouterUneOffre(offre)
    const offres = reservePrimaire.listerLesOffres()
    // Then
    expect(offres).to.deep.equal([
      {
        blocsHoraire: [
          {
            _idParc: "Northwind_3",
            _prixPlancher: 400,
            _quantite: 40,
            creneauHoraireDebut: 0,
            creneauHoraireFin: 2
          },
          {
            _idParc: "Northwind_3",
            _prixPlancher: 400,
            _quantite: 40,
            creneauHoraireDebut: 3,
            creneauHoraireFin: 5
          },
          {
            _idParc: "Icare_02",
            _prixPlancher: 240,
            _quantite: 20,
            creneauHoraireDebut: 6,
            creneauHoraireFin: 8
          },
          {
            _idParc: "Icare_02",
            _prixPlancher: 240,
            _quantite: 20,
            creneauHoraireDebut: 9,
            creneauHoraireFin: 11
          },
          {
            _idParc: "Icare_02",
            _prixPlancher: 240,
            _quantite: 20,
            creneauHoraireDebut: 12,
            creneauHoraireFin: 14
          },
          {
            _idParc: "Icare_02",
            _prixPlancher: 240,
            _quantite: 20,
            creneauHoraireDebut: 15,
            creneauHoraireFin: 17
          },
          {
            _idParc: "Northwind_3",
            _prixPlancher: 400,
            _quantite: 40,
            creneauHoraireDebut: 18,
            creneauHoraireFin: 20
          },
          {
            _idParc: "Northwind_3",
            _prixPlancher: 400,
            _quantite: 40,
            creneauHoraireDebut: 21,
            creneauHoraireFin: 23
          }
        ],
        dateOffre: {
          date: _18_06_2023.date
        },
        id: "OFR_1"
      }
    ])
  })
  it(`doit obtenir la liste des offres sur un marché`, function() {
    // Given
    const reservePrimaire = new Marche(TypeMarche.ReservePrimaire)
    const _18_06_2023 = new DateOnly({ jour: 18, mois: Mois.Juin, annee: 2023 })
    const parcEolien = PARC_EOLIEN(_18_06_2023)
    const parcSolaire = PARC_SOLAIRE(_18_06_2023)
    const offre_1 = new Offre("OFR_1", _18_06_2023)
    offre_1.ajouterBlocHoraire(0, 12, parcEolien)
    const offre_2 = new Offre("OFR_2", _18_06_2023)
    offre_2.ajouterBlocHoraire(6, 18, parcSolaire)
    reservePrimaire.ajouterUneOffre(offre_1)
    reservePrimaire.ajouterUneOffre(offre_2)

    // When
    const offres = reservePrimaire.listerLesOffres()
    // Then
    expect(offres).to.deep.equal([
      {
        id: "OFR_1",
        dateOffre: {
          date: _18_06_2023.date
        },
        blocsHoraire: [
          {
            _idParc: "Northwind_3",
            _prixPlancher: 2400,
            _quantite: 240,
            creneauHoraireDebut: 0,
            creneauHoraireFin: 12
          }
        ]
      },
      {
        id: "OFR_2",
        dateOffre: {
          date: _18_06_2023.date
        },
        blocsHoraire: [
          {
            _idParc: "Icare_02",
            _prixPlancher: 1440,
            _quantite: 120,
            creneauHoraireDebut: 6,
            creneauHoraireFin: 18
          }
        ]
      }
    ])
  })
})


function PARC_EOLIEN(dateProduction: DateOnly) {
  const producteurEolien = new Parc(TypeParc.Eolien, "Northwind_3")
  for (let i = 0; i < 24; i++) {
    const blocHoraire = new BlocHoraireUnitaire(i, 20, 200)
    producteurEolien.ajouterBlocHoraireUnitaire(dateProduction, blocHoraire)
  }
  return producteurEolien
}

function PARC_SOLAIRE(dateProduction: DateOnly) {
  const producteurSolaire = new Parc(TypeParc.Eolien, "Icare_02")
  for (let i = 6; i < 20; i++) {
    const blocHoraire = new BlocHoraireUnitaire(i, 10, 120)
    producteurSolaire.ajouterBlocHoraireUnitaire(dateProduction, blocHoraire)
  }
  return producteurSolaire
}
