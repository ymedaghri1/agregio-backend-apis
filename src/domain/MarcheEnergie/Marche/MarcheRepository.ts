import { Marche, TypeMarche } from "./Marche"
import { Offre } from "../Offre/Offre"

export interface MarcheRepository {
  findOne(type: TypeMarche): Marche | undefined
  findAll(): Marche[]
  addOffre(offre: Offre, marche: Marche): Marche
}
