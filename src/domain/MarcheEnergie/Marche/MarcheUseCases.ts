import { MarcheRepository } from "./MarcheRepository"
import { Marche, TypeMarche } from "./Marche"
import { OffreRepository } from "../Offre/OffreRepository"
import Boom from "@hapi/boom"

export default class MarcheUseCases {
  constructor(private marcheRepository: MarcheRepository, private offreRepository: OffreRepository) {
  }

  getAllMarches(): Marche[] {
    return this.marcheRepository.findAll()
  }

  ajouterOffre(idOffre: string, typeMarche: TypeMarche): Marche {
    const offre = this.offreRepository.findOne(idOffre)
    if (offre === undefined) {
      throw Boom.badRequest("Impossible d'ajouter une offre qui n'existe pas sur un marché")
    }
    const marche = this.marcheRepository.findOne(typeMarche)
    if (marche === undefined) {
      throw Boom.badRequest("Impossible d'ajouter une offre sur un marché inexistant")
    }
    return this.marcheRepository.addOffre(offre, marche)
  }

}
