import { DateOnly } from "../Offre/DateOnly"
import { BlocHoraireUnitaire } from "../Parc/BlocHoraireUnitaire"
import { Offre } from "../Offre/Offre"

export enum TypeMarche {
  ReservePrimaire = "ReservePrimaire",
  ReserveSecondaire = "ReserveSecondaire",
  ReserveRapide = "ReserveRapide"
}


export class Marche {
  constructor(
    public readonly type: TypeMarche,
    private offres: Offre[] = []
  ) {
  }

  ajouterUneOffre(offre: Offre) {
    this.offres.push(offre)
  }

  listerLesOffres() {
    return this.offres
  }
}
