## Périmètre du test technique Agregio Backend APIs
***

Sources : 
- MOOC SmartGrids (Grenoble INP) : https://www.youtube.com/playlist?list=PLrNB1sZV7ngO9pkIlvTSNM-p2md0uaSdB
- Monsieur Bidouille : https://www.youtube.com/c/monsieurbidouille
- ECO CO2 : https://www.ecoco2.com/

***
## RTE : Acteur de l'ajustement pour l'équilibre production consommation

Le mécanisme d'ajustement est un marché organisé ou RTE est l'acheteur des offres proposées

Pour maintenir le réseau à l’équilibre, RTE dispose de réserves de puissance. Ces réserves sont mobilisables, à la hausse ou à la baisse (appel des moyens de production ou effacement), au niveau des installations des utilisateurs du réseau.

**Dans cet exercice on ne va pas s'intéresser aux offres d'effacement mais uniquement aux offres de puissance (capacité).**

**Par ailleurs l'énoncé semble s'appliquer aux sous groupes de la réserve tertiaire et pas aux réserves primaires et secondaires qui font partie des services systèmes et dont le prix est plutôt réglé au prix du marché Spot**

**L'énonçé a été probablement présenté ainsi par soucis de simplification**

***

## Périmètre conçerné et adaptations à l'énonçé

Nous considérerons qu'il existe comme indiqué dans l'énonçé (et par souci de simplification) : 

### Règles métier :
- 3 marchés associés aux réserves Primaire, Secondaire et Rapide.
- Agregio peut placer une offre composée de plusieurs blocs horaires sur chacun de ces marchés
- Une offre peut être constituée de bloc horaires de producteurs différents
- Un bloc horaire peut recouvrir des périodes différentes (1h, 3h, ..)
- Un bloc horaire stipule la quantité d'énergie que le producteur doit fournir et le niveau de rémunération demandé (prix plancher)
- Un bloc horaire d'une offre est produit par un seul producteur d'énergie à la fois
- Les producteurs d'énergie sont de différents types (Solaires, Eoliens ou Hydrauliques)
- Pour des raisons de traçabilité de la production électrique (garantie d'origine), on doit pouvoir connaître le(s) parc(s) qui va(vont) produire l'électricité d'une offre.

### APIS demandées : 
- Création d'une offre
- Création d'un parc
- Lister les offres proposées pour chaque marché
- Lister les parcs qui vendent sur un marché
