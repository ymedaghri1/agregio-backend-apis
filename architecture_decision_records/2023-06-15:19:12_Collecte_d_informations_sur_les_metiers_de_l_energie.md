## Collecte d'informations complémentaires pour comprendre le métier d'aggrégateur
***

Sources : 
- MOOC SmartGrids (Grenoble INP) : https://www.youtube.com/playlist?list=PLrNB1sZV7ngO9pkIlvTSNM-p2md0uaSdB
- Enedis / RTE : https://www.rte-france.com/ , https://data.enedis.fr/pages/accueil/

***
## Acteurs, principes fondamentaux et définitions

### Aggrégateur

Dans le contexte des métiers de l'électricité, un agrégateur est une entité ou une entreprise qui rassemble et gère des ressources de production d'électricité décentralisées.

Il joue généralement le rôle d'intermédiaire entre : 
- des "petits" producteurs d'énergie décentralisés (propriétaires de panneaux solaires ou d'éoliennes)
- des consommateurs d'énergie flexibles (grands sites industriels ou les bâtiments commerciaux)

Son objectif est de maximiser l'utilisation de l'énergie renouvelable, d'optimiser la gestion de la demande et de créer un marché plus efficace pour l'électricité.

### Réserve Primaire
Elle est aussi appelée réserve de fréquence.
Cette réserve est utilisée pour compenser les petites fluctuations dans le réseau électrique qui se produisent en permanence.
Les centrales qui contribuent à la réserve primaire doivent être capables de réagir automatiquement et très rapidement (en quelques secondes) à ces fluctuations. 
Cela implique souvent des centrales qui ont une production contrôlable, comme les centrales hydrauliques, thermiques et nucléaires.

### Réserve Secondaire
Cette réserve est utilisée pour gérer des écarts plus importants et plus durables entre la production et la consommation d'électricité.
Les centrales qui contribuent à la réserve secondaire doivent être capables de modifier leur production en quelques minutes.
Cela peut inclure des centrales hydrauliques, des centrales à gaz et certaines formes d'énergies renouvelables comme l'éolien et le solaire, si elles sont couplées à un stockage d'énergie.

### Réserve Rapide
Cette réserve est utilisée pour répondre à des événements inattendus et importants, tels qu'une panne de centrale.
Les centrales qui contribuent à la réserve rapide doivent être capables de modifier considérablement leur production en très peu de temps (quelques minutes). 
Les centrales hydrauliques, en particulier les centrales hydroélectriques à pompage, sont souvent utilisées pour cette réserve en raison de leur capacité à augmenter ou diminuer rapidement leur production.

### Problème majeur sur le réseau électrique
Le problème majeur pouvant survenir sur un réseau éléctrique est le blackout qui désigne une interruption totale de l'alimentation éléctrique sur une zone géographique donnée (pouvant représenter un pays).
Il est donc indispensable que l'équilibre du réseau électrique soit garanti (Puissance 50Hz) en France et de maitriser les risques de réactions en chaîne lors de fluctuations d'amplitude trop grande et de surchage des lignes à hautes tensions.
Pour cela il doit être possible de jouer sur les capacités de production ou d'effacement 

### Maîtrîse de l'équilibre électrique
Il faut produire en temps réél ce que l'on consomme, il ne peut pas y avoir de décalage entre la production et la consommation.
Par ailleurs la consommation électrique varie tout le temps. Il faut également acheminer l'énergie via les lignes à hautes tension et éviter de les surcharger.
La maîtrise de l'équilibre électrique se base sur l'optimisation de l'adéquation de la production aux besoins futurs de consommation
C'est un exercice complexe reposant sur de multiples mécanismes: 
 - Prévisions météorologiques
 - Diversification géographique 
 - Historique des consommations
 - Stockage d'énergie etc...


