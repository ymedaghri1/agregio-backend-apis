# Agregio Backend Apis

***

## Description
Implémentation d'une api backend pour Agregio dans le cadre d'un entretien technique.

Le projet est une application qui devra servir d'API.
Il sera réalisé en design émergent en fonction des problématiques métier à implémenter.

Dans sa version initiale ce projet est constitué de :
- Un répertoire adr : "Architecture Decision Records"
  - Ce sont les décisions qui sont prises en cours de développement et qui expliquent les choix techniques opérés
  - Ce sont des enregistrement Markdown Immuables (Non modifiés après leur création) afin de retracer fidèlement l'historique des reflexions et des décisions prises au cours de la vie du projet


- Un répertoire "src" représentant l'intégralité du code source de l'application
  - En raison de la complexité métier associée à ce type d'application la paradigme de la "Clean Archi" est également choisi, on trouvera alors également : 
    - Un répertoire "domain" contenant le code métier de l'application agnostique de la technique
    - Un répertoire "infra" qui contiendra le code technique permettant d'interfacer le code métier avec des composants techniques à venir (Bases de données, apis externes, ...)


- Un repertoire docker qui hébergera les services techniques éventuels (persistance, monitoring, cache ..) sous formes de containers utilisables par les développeurs durant les phases de développement


- Un fichier .gitlab-ci.yml pour l'automatisation basique du pipeline de développement


- Des librairies pour l'écriture des tests et les rapports de couverture


- Un outillage custom pour générer une pyramide des tests et vérifier la conformité des tests avec une démarche et des pratiques Craft

Le language utilisé pour le projet est le language TypeScript en raison de son typage solide permettant de détecter rapidement les erreurs liées au type, ce qui rend le code plus fiable et moins sujet aux bugs. 

Aucune autre librairie ou décision technique n'est prise dans cette phase initiale

## Installation

```bash
yarn install
```

## Lancement des tests

Tests unitaires
```bash
yarn test:unit
```
Tests d'intégration
```bash
yarn test:integ
```
Tests fonctionnels
```bash
yarn test:fonc
```

## Lancement de l'application
Phase de développement (via nodemon)
```bash
yarn dev
```

Build de l'application
```bash
yarn build
```

Lancement de l'application via node
```bash
yarn start
```

## Accès au SWAGGER

http://localhost:3000/documentation

![Swagger](architecture_decision_records/images/swagger.png)

## Coding Style
```bash
yarn prettier
```

## Pyramide des tests
```bash
yarn test:pyramid
```
![TestPyramid](architecture_decision_records/images/testPyramid.png)
## Support
Pour toute question vous pouvez me contacter par mail ymedaghri@gmail.com
